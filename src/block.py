import uuid
import  dateutil.parser

from config import NUM_ZEROS, BLOCK_VAR_CONVERSIONS

import validate
import uuid

import dateutil.parser
import validate

from config import NUM_ZEROS, BLOCK_VAR_CONVERSIONS


class Block(object):
    def __init__(self, dictionary):
        '''We're looking for index, timestamp, data, prev_hash, nonce'''

        # We want to support loading from a dict, where the dict may contain string entities
        # or correct types
        for key, value in dictionary.items():
            if key in BLOCK_VAR_CONVERSIONS:
                #If types are the same, set them
                if type(value) == type(BLOCK_VAR_CONVERSIONS[key]):
                    setattr(self, key, value)
                #Else attempt a conversion
                elif key == 'timestamp' and type(value) == str:
                    setattr(self, key, dateutil.parser.parse(value))
                else:
                    setattr(self, key, BLOCK_VAR_CONVERSIONS[key](value))
            #We need this as data type hasn't been set
            else:
                setattr(self, key, value)

        if not hasattr(self, 'nonce'):
            self.nonce = uuid.uuid4().int

        if self.nonce == 'None':
            self.nonce = uuid.uuid4().int

        if not hasattr(self, 'hash'):  # in create the first block, needs to be removed in future
            self.update_hash()

        if self.hash == 'None':
            self.update_hash()

        if not hasattr(self, 'prev_hash'):  # in create the first block, needs to be removed in future
            self.prev_hash = 'None'

        # The loading from dict from timestamp is suspect - add asserts
        # to check that correct types are set
        assert type(self.hash) == BLOCK_VAR_CONVERSIONS['hash']
        assert type(self.data) == BLOCK_VAR_CONVERSIONS['data']
        assert type(self.timestamp) == type(BLOCK_VAR_CONVERSIONS['timestamp'])
        assert type(self.prev_hash) == BLOCK_VAR_CONVERSIONS['prev_hash']
        assert type(self.index) == BLOCK_VAR_CONVERSIONS['index']
        assert type(self.nonce) == BLOCK_VAR_CONVERSIONS['nonce']

    def __dict__(self):
        info = {}
        info['index'] = self.index
        info['timestamp'] = self.timestamp
        info['prev_hash'] = self.prev_hash
        info['hash'] = self.hash
        info['data'] = self.data
        info['nonce'] = self.nonce
        return info

    def __str__(self):
        return "Block<prev_hash: %s,hash: %s, timestamp: %s, data: %s>" % (self.prev_hash,self.hash,self.timestamp,self.data)

    def __repr__(self):  # used for debugging without print, __repr__ > __str__
        return "Block<index: %s>, <hash: %s>" % (self.index, self.hash)

    def __eq__(self, other):
        return (self.index == other.index and
                self.timestamp == other.timestamp and
                self.prev_hash == other.prev_hash and
                self.hash == other.hash and
                self.data == other.data and
                self.nonce == other.nonce)

    def __ne__(self, other):
        return not self.__eq__(other)

    # Used for python sorting
    def __lt__(self, other):
        return self.index < other.index

    def update_hash(self):
        hash = self.calculate_hash()
        self.hash = hash
        return hash

    def calculate_hash(self):
        return validate.calculate_hash(index=self.index,
                                 prev_hash=self.prev_hash,
                                 data=self.data,
                                 timestamp=self.timestamp,
                                 nonce=self.nonce)

    def generate_header(self):
        return validate.generate_header(index=self.index,
                                 prev_hash=self.prev_hash,
                                 data=self.data,
                                 timestamp=self.timestamp,
                                 nonce=self.nonce)

    def is_valid(self):
        '''Current validity is that the has begins with at least NUM_ZEROS'''
        if self.calculate_hash() != self.hash:
            return False

        if str(self.hash[0:NUM_ZEROS]) == '0' * NUM_ZEROS:
          return True
        else:
          return False
