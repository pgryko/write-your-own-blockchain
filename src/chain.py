import disk

class Chain(object):
    '''A block chain, is defined as a dict of block objects'''
    def __init__(self, blocks: dict):
        self.blocks = blocks
        # As __lt__ is defined for block, we can sort the blocks by index
        self.blocks.sort()

    def is_valid(self):
        '''Validates blockchain

        Blockchain is valid if:
        1) Each block is indexed one after the other
        2) Each block previous hash is the hash of the prev block
        3) The blocks has is valid for the no. of zeros
        '''

        for index, cur_block in enumerate(self.blocks[1:]):
            prev_block = self.blocks[index]
            if prev_block.index+1 != cur_block.index:
                return False
            if not cur_block.is_valid():
                return False
            if prev_block.hash != cur_block.prev_hash:
                return False
        return True

    def save(self, chaindata_dir = None):
        '''
        Save the block chain to file system
        :return: boolean
        '''
        for b in self.blocks:
            disk.save_block(b,chaindata_dir)
        return True

    def load(self,chaindata_dir = None):
        self.blocks = disk.load_blocks(chaindata_dir)

    def find_block_by_hash(self,hash):
        #Bah, inefficient, should really use a hash map
        for b in self.blocks:
            if b.hash == hash:
                return b
        return False

    def __len__(self):
        return len(self.blocks)

    def __eq__(self, other):
        if len(self) != len(other):
            return False
        for self_block, other_block in zip(self.blocks,other.blocks):
            if self_block != other_block:
                return False
        return True

    def __gt__(self, other):
        return len(self.blocks) > len(other.blocks)

    def __lt__(self, other):
        return len(self.blocks) < len(other.blocks)

    def __ge__(self,other):
        return self.__eq__(other) or self.__gt__(other)

    def max_index(self):
        if self.blocks:
            return self.blocks[-1].index

    def add_block(self,new_block):
        '''
        Append new block to index
        Todo: Insert block into index that is requested.
        If the index is one that currently exists, the new block would take its place,
        if the new block is valid.
        Else discard new block and return False
        :param new_block:
        :return: boolean
        '''

        self.blocks.append(new_block)
        return True

    def block_list_dict(self):
        return [b.to_dict() for b in self.blocks]