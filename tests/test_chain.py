import json
import os, sys
import shutil

myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../src')

import block
import chain
from disk import save_block, load_blocks

block_zero = {"hash": "0000046e0526dcaa41b10b699cdcfc0d991cbcfe6185a8b16ced38bb28f52e99", "index": 0, "data": "First block data", "prev_hash": "None", "timestamp": "2017-12-01 18:07:52.504019", "nonce": 1855986}

block_one = {"hash": "00000ec6650817de7dd7a68c9872290837246cd8288dc93c34c38ae70af5631b", "index": 1, "data": "I block #1", "prev_hash": "0000046e0526dcaa41b10b699cdcfc0d991cbcfe6185a8b16ced38bb28f52e99", "timestamp": "2017-12-01 19:08:02.144858", "nonce": 216052038965204308110149004868451327843}

block_two = {"timestamp": "2017-12-01 19:08:03.617121", "hash": "000007cf2de2fe06f8087eeed9755ab4d7e43b1c446f0069ae5522ff906e9462", "index": 2, "data": "I block #2", "nonce": 213185635377339708621667182486587066382, "prev_hash": "00000ec6650817de7dd7a68c9872290837246cd8288dc93c34c38ae70af5631b"}

block_three = {"hash": "00000f0b776c30159649540f54d37993c499448497074a0351bb6212f47d2aa1", "nonce": 190716929492229888395196740106253758696, "index": 3, "prev_hash": "000007cf2de2fe06f8087eeed9755ab4d7e43b1c446f0069ae5522ff906e9462", "timestamp": "2017-12-01 19:17:57.315747", "data": "I block #3"}

class TestClass:

    curr_dir = os.path.dirname(os.path.abspath(__file__))
    chaindata_dir = os.path.join(curr_dir, 'chaindata_dir')

    def setup_method(self, method):
        if not os.path.exists(self.chaindata_dir):
            os.mkdir(self.chaindata_dir)

    def teardown_method(self, method):
        '''Ensure chaindata_dir is removed once a test passes'''
        if os.path.exists(self.chaindata_dir):
            shutil.rmtree(self.chaindata_dir)


    def test_create_empty_chain(self):
        '''Test the initialisation of an empty chain'''
        new_chain = chain.Chain([])

        # Should an empty chain be considered valid?
        assert new_chain.is_valid()

        #Test that save function doesn't crash with empty block
        new_chain.save(self.chaindata_dir)

        #Check that size is zero
        assert len(new_chain) == 0

        #Add new block
        new_chain.add_block(block_zero)

        #Check that new size is 1
        assert len(new_chain) == 1

        #Add new block
        new_chain.add_block(block_one)

        #Check that new size is 1
        assert len(new_chain) == 2

    def test_create_chain(self):
        '''Test the initialisation of an empty chain'''

        blocks = [block.Block(block_zero),
                  block.Block(block_one),
                  block.Block(block_two)]

        new_chain = chain.Chain(blocks)

        assert new_chain.is_valid()

        new_chain.save(self.chaindata_dir)

        # Check that size is 3
        assert len(new_chain) == 3

        # Add new block
        new_chain.add_block(block_three)

        # Check that new size is 4
        assert len(new_chain) == 4

    def test_save_chain(self):
        '''Test saving and loading chain'''
        blocks = [block.Block(block_zero),
                  block.Block(block_one),
                  block.Block(block_two),
                  block.Block(block_three)]

        first_chain = chain.Chain(blocks)

        assert first_chain.is_valid()


        first_chain.save(self.chaindata_dir)

        second_chain = chain.Chain([])

        second_chain.load(self.chaindata_dir)

        assert first_chain == second_chain

    def test_block_sorting(self):
        '''Test that blocks are sorted correctly when loaded into
        a chain'''
        blocks = [block.Block(block_three),
                  block.Block(block_one),
                  block.Block(block_two),
                  block.Block(block_zero)]

        first_chain = chain.Chain(blocks)

        assert first_chain.is_valid()

        assert first_chain.blocks[0].index ==  0
        assert first_chain.blocks[1].index ==  1
        assert first_chain.blocks[2].index ==  2
