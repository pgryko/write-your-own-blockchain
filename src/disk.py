"""
Various functions for saving, reading and validating blocks saved to file system.
"""
import json
import os

from block import Block
from config import CHAINDATA_DIR


def get_file_list(chaindata_dir = None):
    '''Returns a sorted list of files with .json extension'''
    if not chaindata_dir:
        chaindata_dir = CHAINDATA_DIR

    if os.path.exists(chaindata_dir):
        file_list = [f for f in os.listdir(chaindata_dir) if f.endswith('.json')]
        file_list.sort()

        return file_list

def save_block(block: Block, chaindata_dir = None):

    #Hmm, might change this to a warming
    if not block.is_valid():
        raise IOError('Attempting to save invalid block file ')

    if not chaindata_dir:
        chaindata_dir = CHAINDATA_DIR

    # Front of zeros so they stay in numerical order
    index_string = str(block.index).zfill(6)
    filename = '%s/%s.json' % (chaindata_dir,index_string)
    with open(filename,'w') as block_file:

        #Should really use pickle module rather than json dump/load
        block_dict = block.__dict__()
        block_dict['timestamp'] = str(block_dict['timestamp'])
        json.dump(block_dict,block_file)

def load_blocks(chaindata_dir = None):

    if not chaindata_dir:
        chaindata_dir = CHAINDATA_DIR

    node_blocks = []
    file_list = get_file_list(chaindata_dir)

    #If no files are created, return empty list
    if not file_list:
        return []
    for filename in file_list:
        filepath = '%s/%s' % (chaindata_dir,filename)
        with open(filepath, 'r') as block_file:
            block_info = json.load(block_file)
            block_object = Block(block_info) # Since we can init a block object from just a dict

            #Were going to check if block's are valid when loading
            if not block_object.is_valid():
                raise IOError('Loaded blockfile ' + block_file + ' has failed a hash check')
            node_blocks.append(block_object)

    node_blocks.sort()
    return node_blocks

def create_chaindata_dir(chaindata_dir = None):

    if not chaindata_dir:
        chaindata_dir = CHAINDATA_DIR

    if not os.path.exists(chaindata_dir):
        os.mkdir(chaindata_dir)

if __name__ == '__main__':
    blocks = load_blocks()
    print ([block.index for block in blocks])
