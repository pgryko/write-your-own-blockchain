import datetime as date
import hashlib
import uuid

import disk
from block import Block
from disk import save_block
from config import CHAINDATA_DIR
import validate

NUM_ZEROS = 5

def iterate_hash(block):

    while str(block.update_hash()[0:NUM_ZEROS]) != '0' * NUM_ZEROS:
        block.nonce += 1
    return block


def mine(last_block):
    #Todo: add timeout if calcuation takes too long
    block_data = {}
    block_data['index'] = int(last_block.index) + 1
    block_data['timestamp'] = date.datetime.now()
    block_data['data'] = "I block #%s" % (int(last_block.index) + 1)
    block_data['prev_hash'] = last_block.hash
    block_data['nonce'] = uuid.uuid4().int
    block = Block(block_data)
    iterate_hash(block)

    return block

def create_first_block():
    #index zero and arbitrary previous hash
    block_data = {}
    block_data['index'] = 0
    block_data['timestamp'] = date.datetime.utcnow()
    block_data['prev_hash'] = None
    block_data['data'] = 'First block data'
    block_data['nonce'] = 0
    block = Block(block_data)
    iterate_hash(block)

    return block


if __name__ == '__main__':
  node_blocks = disk.load_blocks()
  if not node_blocks:
      disk.create_chaindata_dir()
      first_block = create_first_block()
      save_block(first_block)

  node_blocks = disk.load_blocks()
  last_block = node_blocks[-1]
  new_block = mine(last_block)
  save_block(new_block)