import dateutil.parser
import sys, os

myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../src')

import block

block_dict = {'hash': 'd7e7257f094ca6d3d5275222b563b285fd42eba03bf2981901119d15240edd86', 'data': 'First block data',
                'timestamp': dateutil.parser.parse('2017-11-14 18:40:50.749797'), 'prev_hash': 'None', 'index': int(0), 'nonce': int(0)}

block_string = {'hash': 'd7e7257f094ca6d3d5275222b563b285fd42eba03bf2981901119d15240edd86', 'data': 'First block data',
                'timestamp': '2017-11-14 18:40:50.749797', 'prev_hash': 'None', 'index': '0', 'nonce': '0'}

block_none = {'hash': 'd7e7257f094ca6d3d5275222b563b285fd42eba03bf2981901119d15240edd86', 'data': 'First block data',
                'timestamp': dateutil.parser.parse('2017-11-14 18:40:50.749797'), 'prev_hash': None, 'index': int(0), 'nonce': int(0)}


valid_block = {"hash": "0000046e0526dcaa41b10b699cdcfc0d991cbcfe6185a8b16ced38bb28f52e99", "index": 0, "data": "First block data", "prev_hash": "None", "timestamp": "2017-12-01 18:07:52.504019", "nonce": 1855986}

invalid_block = {"data": "First block data", "prev_hash": "None", "nonce": "1", "index": "0", "timestamp": "2017-11-18 18:12:36.455067", "hash": "100003e0f30c0cced86fc409048f01e8f7e4962519ec8267c65dd2b5fa6bf429"}


def test_block_dict():
    '''Check that a block can be created from dic,
        where all values are correct types'''
    first_block = block.Block(block_dict)
    assert first_block.__dict__() == block_dict
    assert block.Block(first_block.__dict__()) == first_block
    assert block.Block(first_block.__dict__()).__dict__() == first_block.__dict__()


def test_block_json():
    '''Check that a block can be created from string dic'''
    first_block = block.Block(block_string)
    assert first_block.__dict__() == block_dict
    assert block.Block(first_block.__dict__()) == first_block
    assert block.Block(first_block.__dict__()).__dict__() == first_block.__dict__()

def test_block_none():
    '''Check the case where we pass in a none value'''
    first_block = block.Block(block_none)
    assert first_block.__dict__() == block_dict
    assert block.Block(first_block.__dict__()) == first_block
    assert block.Block(first_block.__dict__()).__dict__() == first_block.__dict__()


def test_is_valid():
    '''Check that a block fails validation if hash is wrong'''
    first_block = block.Block(valid_block)
    assert first_block.is_valid()


def test_fail_is_valid():
    '''Check that a block fails validation if hash is wrong'''
    first_block = block.Block(invalid_block)
    assert not first_block.is_valid()

