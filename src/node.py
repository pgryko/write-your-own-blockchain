import json

from flask import Flask

import disk

node = Flask(__name__)

node_blocks = disk.load_blocks() #Initial blocks that are synced

@node.route('/blockchain.json',methods=['Get'])
def blockchain():
    '''
    Outputs the blockchain, which in our case, is a list of json hashes
    with block information

    :return: json.dumps{
    index,
    timestamp,
    data,
    hash,
    prev_hash
    }
    '''
    node_blocks = disk.load_blocks()  # Re-grab the nodes if they've changes

    python_blocks = []
    for block in node_blocks:
        python_blocks.append(block.__dict__())

    return json.dumps(python_blocks)

if __name__ == '__main__':
    node.run()