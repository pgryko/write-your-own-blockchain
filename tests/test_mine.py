import os, sys
import shutil

myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../src')


import mine

import disk

# block_zero = {"data": "First block data", "prev_hash": "None", "nonce": "151917", "index": "0", "timestamp": "2017-11-18 18:12:36.455067", "hash": "000003e0f30c0cced86fc409048f01e8f7e4962519ec8267c65dd2b5fa6bf429"}
#
# block_one = {"data": "I block #1", "prev_hash": "000003e0f30c0cced86fc409048f01e8f7e4962519ec8267c65dd2b5fa6bf429", "nonce": "335559172545230996667722609778124461085", "index": "1", "timestamp": "2017-11-18 20:12:48.190261", "hash": "0000019b6ccd69181f8d8ad7ce10be00e5a13127debcbd0c596bc60952f39021"}
#
# block_two = {"index": "2", "hash": "000008e2850c26941e55645e6bbe44dacc899a848f824518090e69a3d77f92b7", "nonce": "44476310464199624619141816015328441654", "data": "I block #2", "timestamp": "2017-11-18 20:27:49.473239", "prev_hash": "0000019b6ccd69181f8d8ad7ce10be00e5a13127debcbd0c596bc60952f39021"}
#
# block_three = {"timestamp": "2017-11-18 20:27:55.153524", "nonce": "173688964446592292785946934368099921905", "index": "3", "data": "I block #3", "prev_hash": "000008e2850c26941e55645e6bbe44dacc899a848f824518090e69a3d77f92b7", "hash": "0000074a80f58c3767b18d624bf9b7057b727723c75e132b4f6eab38e6e9537d"}


class TestClass:

    curr_dir = os.path.dirname(os.path.abspath(__file__))
    chaindata_dir = os.path.join(curr_dir, 'chaindata_dir')

    def setup_method(self, method):
        if not os.path.exists(self.chaindata_dir):
            os.mkdir(self.chaindata_dir)

    def teardown_method(self, method):
        '''Ensure chaindata_dir is removed once a test passes'''
        if os.path.exists(self.chaindata_dir):
            shutil.rmtree(self.chaindata_dir)

    def test_create_first(self):
        '''Attempts to mine initial block'''
        block_zero = mine.create_first_block()

        #Assert that the block hash is correct

        assert block_zero.hash == block_zero.calculate_hash()
        assert block_zero.is_valid()


    def test_save_block(self):

        node_blocks = disk.load_blocks(self.chaindata_dir)
        if not node_blocks:
            disk.create_chaindata_dir(self.chaindata_dir)
            first_block = mine.create_first_block()
            disk.save_block(first_block, self.chaindata_dir)

        node_blocks = disk.load_blocks(self.chaindata_dir)
        last_block = node_blocks[-1]
        new_block = mine.mine(last_block)
        disk.save_block(new_block, self.chaindata_dir)


