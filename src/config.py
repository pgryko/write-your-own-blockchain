import os
from datetime import datetime

_cur_dir = os.path.dirname(os.path.abspath(__file__))

_project_dir = os.path.abspath(os.path.join(_cur_dir, '../'))


CHAINDATA_DIR = os.environ.get('CHAINDATA_DIR', os.path.join(_project_dir, 'chaindata_dir'))
BROADCASTED_BLOCK_DIR = os.path.join(CHAINDATA_DIR, 'bblocs/')

NUM_ZEROS = 5 #difficulty, currently.

BLOCK_VAR_CONVERSIONS = {'index': int, 'nonce': int,
                         'hash': str, 'prev_hash': str,
                         'timestamp': datetime.utcnow(),
                         'data': str}

if __name__ == '__main__':
    print(_project_dir)