import json
import os, sys
import shutil

myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../src')

import block
from disk import save_block, load_blocks

block_zero = {"hash": "0000046e0526dcaa41b10b699cdcfc0d991cbcfe6185a8b16ced38bb28f52e99", "index": 0, "data": "First block data", "prev_hash": "None", "timestamp": "2017-12-01 18:07:52.504019", "nonce": 1855986}

block_one = {"hash": "00000ec6650817de7dd7a68c9872290837246cd8288dc93c34c38ae70af5631b", "index": 1, "data": "I block #1", "prev_hash": "0000046e0526dcaa41b10b699cdcfc0d991cbcfe6185a8b16ced38bb28f52e99", "timestamp": "2017-12-01 19:08:02.144858", "nonce": 216052038965204308110149004868451327843}

block_two = {"timestamp": "2017-12-01 19:08:03.617121", "hash": "000007cf2de2fe06f8087eeed9755ab4d7e43b1c446f0069ae5522ff906e9462", "index": 2, "data": "I block #2", "nonce": 213185635377339708621667182486587066382, "prev_hash": "00000ec6650817de7dd7a68c9872290837246cd8288dc93c34c38ae70af5631b"}

block_three = {"hash": "00000f0b776c30159649540f54d37993c499448497074a0351bb6212f47d2aa1", "nonce": 190716929492229888395196740106253758696, "index": 3, "prev_hash": "000007cf2de2fe06f8087eeed9755ab4d7e43b1c446f0069ae5522ff906e9462", "timestamp": "2017-12-01 19:17:57.315747", "data": "I block #3"}

class TestClass:

    curr_dir = os.path.dirname(os.path.abspath(__file__))
    chaindata_dir = os.path.join(curr_dir, 'chaindata_dir')

    def setup_method(self, method):
        if not os.path.exists(self.chaindata_dir):
            os.mkdir(self.chaindata_dir)

    def teardown_method(self, method):
        '''Ensure chaindata_dir is removed once a test passes'''
        if os.path.exists(self.chaindata_dir):
            shutil.rmtree(self.chaindata_dir)

    def test_save_block(self):
        '''Test that saving a single block works'''


        '''Create data dir folder'''
        if not os.path.exists(self.chaindata_dir):
            os.mkdir(self.chaindata_dir)

        first_block = block.Block(block_zero)

        save_block(first_block,self.chaindata_dir)

        assert os.path.exists(os.path.join(self.chaindata_dir, '000000.json'))

        '''Test that file has been written correctly'''

        block_info = json.load(open(os.path.join(self.chaindata_dir, '000000.json')))
        block_object = block.Block(block_info)  # Since we can init a block object from just a dict

        assert first_block == block_object

    def test_sync_block(self):
        '''Write multiple blocks to file and test loading them'''

        reference_blockchain = [block.Block(block_zero),
                                block.Block(block_one),
                                block.Block(block_two),
                                block.Block(block_three)]

        for block_item in reference_blockchain:
            save_block(block_item, self.chaindata_dir)

        loaded_blockchain = load_blocks(self.chaindata_dir)

        assert [block_item.index for block_item in loaded_blockchain] == \
               [block_item.index for block_item in reference_blockchain]

        assert loaded_blockchain == reference_blockchain

